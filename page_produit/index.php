<html>

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <title>Your Website</title>
</head>

<body>
<?php
ini_set('display_errors', 1);
header ('Content-type: text/html; charset=utf-8');

$servername = "pj630578-001.privatesql";
$username = "usercabatho";
$password = "GUYfgy54fd541";
$dbname = "dbcabatho";
$port ='35735';

// Create connection
$conn = new mysqli($servername, $username, $password, $dbname,$port);

// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}
if (isset($_POST['submit']) && isset($_FILES)) {
    $FileType = strtolower(pathinfo($_FILES["xlsx_file"]["name"], PATHINFO_EXTENSION));
    $target_file = "xlsx_uploaded/" . $_FILES["xlsx_file"]["name"];
    if ($FileType == "xlsx") {
        if (move_uploaded_file($_FILES["xlsx_file"]["tmp_name"], $target_file)) {
            save_file($_FILES["xlsx_file"]["name"], $conn);
        } else {
            header('HTTP/1.0 403 Forbidden');
            echo "Sorry, there was an error uploading your file.";
        }
    } else {
        echo "Veuillez choisir un fichier de type xlsx";
    }
} else {
}
?>
<link rel="stylesheet"
      href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>
<div class="pl-5 mt-4">
    <div class="row">
        <div class="w-100"><h1>Importer les donn&eacute;es</h1></div>
        <div class="w-100 row">
            <div class="col">
                <form method="POST"
                      action=""
                      enctype="multipart/form-data">
                    <input type="file"
                           name="xlsx_file"/>
                    <input type="submit"
                           name="submit"
                           value="Valider"
                           class="btn btn-success"/>
                </form>
            </div>
            <div class="col">
                <a href="import_to_presta.php"><button class="btn btn-secondary">Importer vers prestashop</button></a>
            </div>
            <div class="col">
                <a href="javascript:void(0);" class="btn btn-danger" onclick="javascript:confirm_remove();">Supprimer toutes les données</a>
                <script type="application/javascript">
                    function confirm_remove(){
                        if (confirm('Voulez-vous supprimer toutes les données ?')) {
                            window.location.href = "removeall.php";
                        }
                    }
                </script>
            </div>
            <div class="col">
                <a href="Structure_catalogue.xlsx" class="btn btn-info">Format Fichier Excel Exemple</a>
            </div>

        </div>
    </div>
</div>
<?php
function save_file($file_name, $conn)
{
    $rownb = 0;
    $allpath = scandir('datas');
    require_once('vendor/SimpleXLSX.php');
    if ($xlsx = SimpleXLSX::parse("xlsx_uploaded/" . $file_name)) {
        $allparsed = $xlsx->rows();
        foreach ($allparsed as $path) {
            if ($rownb > 0) {
                $to_save = [
                    $path[0], //Nom_Produit
                    $path[1], //Univers
                    $path[2], //Marque
                    $path[3],  //Type
                    $path[4], //Modele
                    $path[5], //Reference
                    $path[6], //Numero_de_serie
                    $path[7], //Description
                    $path[8], //Nom_Piece
                    $path[9], //Type_Piece
                    $path[10], //Reference_Piece
                    $path[11], //Prix
                    $path[12], //Stock
                    $path[13], //Description_Piece
                    $path[14], //nombre_requis
                    $path[15], //Repere_vue_eclatee
                ];
                /*$verify_sql = "SELECT nom_produit FROM produit WHERE nom_produit = '$path[0]' ";
                $result_verif = $conn->query($verify_sql);
                if (count($result_verif->fetch_assoc()) == 0) {*/

                //$sql = "INSERT INTO produit (nom_produit, categorie, quantite, image, marque, modele, type_produit, description, vue_eclate,sku) VALUES ('$path[0]', '$path[1]', '$path[2]', '$path[3]', '$path[4]', '$path[5]', '$path[6]', '$path[7]', '$path[8]','$path[9]')";
                $sql = "INSERT INTO `structure_catalogue` (`Nom_Produit`, `Univers`, `Marque`, `Type`, `Modele`, `Reference`, `Numero_de_serie`, `Description`, `Nom_Piece`, `Type_Piece`, `Reference_Piece`, `Prix`, `Stock`, `Description_Piece`, `nombre_requis`, `Repere_vue_eclatee`, `Nom_Fichier`, `date_upload`) 
                            VALUES ('$path[0]', '$path[1]', '$path[2]', '$path[3]', '$path[4]', '$path[5]', '$path[6]', '$path[7]', '$path[8]', '$path[9]', '$path[10]', '$path[11]', '$path[12]', '$path[13]', '$path[14]', '$path[15]', '$file_name', current_timestamp());";

                if ($conn->query($sql) === TRUE) {
                    //echo "New record created successfully";
                } else {
                    //echo "Error: " . $sql . "<br>" . $conn->error;
                }

            }
            $rownb++;
        }
    } else {
        echo SimpleXLSX::parseError();
    }
}

$list_query = "SELECT * FROM structure_catalogue";
$preslist = $conn->query($list_query);
?>
<table class="table">
    <thead>
    <tr>
        <th scope="col">
            Num
        </th>
        <th scope="col">
            Nom_Produit
        </th>
        <th scope="col">
            Images
            Produit
        </th>
        <th scope="col">
            Images
            Vue
            Eclatée
        </th>
        <th scope="col">
            Univers
        </th>
        <th scope="col">
            Marque
        </th>
        <th scope="col">
            Type
        </th>
        <th scope="col">
            Modele
        </th>
        <th scope="col">
            Reference
        </th>
        <th scope="col">
            Numero_de_serie
        </th>
        <th scope="col">
            Description
        </th>
        <th scope="col">
            Nom_Piece
        </th>
        <th scope="col">
            Type_Piece
        </th>
        <th scope="col">
            Reference_Piece
        </th>
        <th scope="col">
            Prix
        </th>
        <th scope="col">
            Stock
        </th>
        <th scope="col">
            Description_Piece
        </th>
        <th scope="col">
            nombre_requis
        </th>
        <th scope="col">
            Repere_vue_eclatee
        </th>
        <th scope="col">
            Action
        </th>
    </tr>
    </thead>
    <tbody>
    <?php while ($items = $preslist->fetch_assoc()) { ?>
        <tr>
            <td><?php echo $items['id']; ?></td>
            <td><?php echo str_replace('é','&eacute;',$items['Nom_Produit']); ?></td>
            <td>
                <button type="button"
                        class="btn btn-primary"
                        data-toggle="modal"
                        data-target="#myModal<?php echo $items['id']; ?>">
                    voir
                    image
                </button>
            </td>
            <td>
                <button type="button"
                        class="btn btn-primary"
                        data-toggle="modal"
                        data-target="#myModalEclated<?php echo $items['id']; ?>">
                    voir
                    image
                </button>
            </td>
            <td><?php echo $items['Univers']; ?></td>
            <td><?php echo $items['Marque']; ?></td>
            <td><?php echo $items['Type']; ?></td>
            <td><?php echo $items['Modele']; ?></td>
            <td><?php echo $items['Reference']; ?></td>
            <td><?php echo $items['Numero_de_serie']; ?></td>
            <td><?php echo $items['Description']; ?></td>
            <td><?php echo $items['Nom_Piece']; ?></td>
            <td><?php echo $items['Type_Piece']; ?></td>
            <td><?php echo $items['Reference_Piece']; ?></td>
            <td><?php echo $items['Prix']; ?></td>
            <td><?php echo $items['Stock']; ?></td>
            <td><?php echo $items['Description_Piece']; ?></td>
            <td><?php echo $items['nombre_requis']; ?></td>
            <td><?php echo $items['Repere_vue_eclatee']; ?></td>
        </tr>
        <div class="modal"
             id="myModal<?php echo $items['id']; ?>">
            <div class="modal-dialog">
                <div class="modal-content">

                    <!-- Modal Header -->
                    <div class="modal-header">
                        <h4 class="modal-title">
                            Images <?php echo $items['Nom_Produit']; ?></h4>
                        <button type="button"
                                class="close"
                                data-dismiss="modal">
                            &times;
                        </button>
                    </div>

                    <!-- Modal body -->
                    <div class="modal-body">
                        <?php
                        $path_dir = "Archive/" . $items['Modele'] . "/images/";
                        try {
                            $dir_content = scandir($path_dir);
                            foreach ($dir_content as $content) {
                                if (isset($content) && $content != '.' && $content != '..') {
                                    ?>
                                    <img class="img-fluid w-100"
                                         src="<?php echo $path_dir . $content; ?>">
                                    <?php
                                }
                            }
                        } catch (Exception $e) {
                            echo 'Exception reçue : ',  $e->getMessage(), "\n";
                        }
                        ?>
                    </div>

                    <!-- Modal footer -->
                    <div class="modal-footer">
                        <button type="button"
                                class="btn btn-danger"
                                data-dismiss="modal">
                            Close
                        </button>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal"
             id="myModalEclated<?php echo $items['id']; ?>">
            <div class="modal-dialog">
                <div class="modal-content">

                    <!-- Modal Header -->
                    <div class="modal-header">
                        <h4 class="modal-title">
                            Vue
                            éclatée <?php echo $items['Nom_Produit']; ?></h4>
                        <button type="button"
                                class="close"
                                data-dismiss="modal">
                            &times;
                        </button>
                    </div>

                    <!-- Modal body -->
                    <div class="modal-body">
                        <?php
                        $path_dir = "Archive/" . $items['Modele'] . "/vue/";
                        try {
                            $dir_content = scandir($path_dir);
                            foreach ($dir_content as $content) {
                                if (isset($content) && $content != '.' && $content != '..') {
                                    ?>
                                    <img class="img-fluid w-100"
                                         src="<?php echo $path_dir . $content; ?>">
                                    <?php
                                }
                            }
                        } catch (Exception $e) {
                            echo 'Exception reçue : ',  $e->getMessage(), "\n";
                        }
                        ?>
                    </div>

                    <!-- Modal footer -->
                    <div class="modal-footer">
                        <button type="button"
                                class="btn btn-danger"
                                data-dismiss="modal">
                            Close
                        </button>
                    </div>
                </div>
            </div>
        </div>
    <?php } ?>
    </tbody>
</table>
</body>

</html>