<?php
if(isset($_POST['submit']) && isset($_FILES)) {
    require __DIR__ . '/vendor/autoload.php';
    $target_dir = "uploads/";
    $uploadOk = 1;
    $FileType = strtolower(pathinfo($_FILES["attachment"]["name"],PATHINFO_EXTENSION));
    $randomstr = generateRandomString();
    $target_file = $target_dir . $_FILES["attachment"]["name"];
    // Check file size
    if ($_FILES["attachment"]["size"] > 5000000) {
        header('HTTP/1.0 403 Forbidden');
        echo "Sorry, your file is too large.";
        $uploadOk = 0;
    }
    if($FileType != "pdf" && $FileType != "png" && $FileType != "jpg" && $FileType != "gif") {

        header('HTTP/1.0 403 Forbidden');
        echo "Sorry, please upload a png or pdf file";
        $uploadOk = 0;

    }
    if ($uploadOk == 1) {
   
        if (move_uploaded_file($_FILES["attachment"]["tmp_name"], $target_file)) {
            // $src = "/home/priviconfw/www/randevteam/prestashop16123/".$target_file;  // source folder or file
            // $dest = "/home/priviconfw/www/randevteam/prestashop16123/engine2/uploads";   // destination folder or file        
            // shell_exec("cp -r $src $dest");
            

            uploadToApi($target_file,$_FILES["attachment"]["name"]);

        } else {
            header('HTTP/1.0 403 Forbidden');
            echo "Sorry, there was an error uploading your file.";
        }
    } 
} else {
    header('HTTP/1.0 403 Forbidden');
    echo "Sorry, please upload a pdf file";
}

//upload fichier moteur1 desactive

function uploadToApi($target_file,$filename){
    require __DIR__ . '/vendor/autoload.php';
    $fileData = fopen($target_file, 'r');

    $client = new \GuzzleHttp\Client();
    try {
    $r = $client->request('POST', 'https://apipro2.ocr.space/parse/image',[
        'headers' => ['apiKey' => 'PKMXB8221888A'],
        'multipart' => [
            [
                'name' => 'file',
                'contents' => $fileData,
            ],
            [
                'name' => 'isOverlayRequired',
                'contents' => "true",
            ]
        ]
    ], ['file' => $fileData]);
     $response =  json_decode($r->getBody(),true);
    
     $word = array_merge(range('a', 'z'), range('A', 'Z'));
     shuffle($word);
  
    $id_token = substr(implode($word), 0, 5);
   
    if($response['ErrorMessage'] == "") { 
        
        $result_to_database = "";
        foreach($response['ParsedResults'] as $pareValue) {
            
            $result_to_database .= $pareValue['ParsedText']." ";
            foreach($pareValue as $res){
             
                foreach($res['Lines'] as $to_save){


                    $MaxHeight = $to_save['MaxHeight'];
                    $MinTop = $to_save['MinTop'];
                    $LineText = $to_save['LineText'];
                    $WordText = preg_replace('/-/', '', $to_save['Words'][0]['WordText']);
                    $Left = $to_save['Words'][0]['Left'];
                    $Top= $to_save['Words'][0]['Top'];
                    $Height = $to_save['Words'][0]['Height'];
                    $Width = $to_save['Words'][0]['Width'];
                    $datetime = date('Y-m-d H:i:s');

                        $to_save_field = ",'".$LineText."','".$WordText."','".$Left."','".$Top."','".$Height."','".$Width;
                        $servername = "pj630578-001.privatesql";

$username = "usercabatho";

$password = "GUYfgy54fd541";

$dbname = "dbcabatho";

$port ='35735';

$link=mysqli_connect($servername, $username, $password, $dbname,$port);
                        if($link === false){
                            die("ERROR: Could not connect. " . mysqli_connect_error());
                        }
                        // foreach()
                        if($WordText != "" && $WordText !=" "){
                         $sql = "INSERT INTO `ocr_cabatho_data` (`id`,`id_save`, `data`,`file_name`, `datetime`,`LineText`,`WordText`,`pos_Left`,`pos_Top`,`pos_Height`,`Width`,`MaxHeight`,`MinTop`) VALUES (NULL,'".$target_file."','".str_replace("'","",$result_to_database)."','".$filename."','".$datetime."'".$to_save_field."','".$MaxHeight."','".$MinTop."')";
                        if(mysqli_query($link, $sql)){
                            //echo "Records inserted successfully.";
                        } else{
                            //echo "ERROR: Could not able to execute $sql. " . mysqli_error($link);
                        }
                    }
                }
            }
            
        }
        
        
        mysqli_close($link);
        uploadToApimoteur1_active($target_file,$filename);


?>
<?php

    }
    } catch(Exception $err) {
        header('HTTP/1.0 403 Forbidden');
        echo $err->getMessage();
    }
}

///Upload fichier moteur1 active

function uploadToApimoteur1_active($target_file,$filename){
    require __DIR__ . '/vendor/autoload.php';
    $fileData = fopen($target_file, 'r');
    $client = new \GuzzleHttp\Client();
    try {
    $r = $client->request('POST', 'https://apipro2.ocr.space/parse/image',[
        'headers' => ['apiKey' => 'PKMXB8221888A'],
        'multipart' => [
            [
                'name' => 'file',
                'contents' => $fileData,
            ],
            [
                'name' => 'isOverlayRequired',
                'contents' => "true",
            ],
            [
                'name' => 'scale',
                'contents' => "true",
            ]
        ]
    ], ['file' => $fileData]);
     $response =  json_decode($r->getBody(),true);

    
     $word = array_merge(range('a', 'z'), range('A', 'Z'));
                    shuffle($word);
                    $id_token = substr(implode($word), 0, 5);
    if($response['ErrorMessage'] == "") {
        $result_to_database = "";
        foreach($response['ParsedResults'] as $pareValue) {
     
            $result_to_database .= $pareValue['ParsedText']." ";
            foreach($pareValue as $res){

              
                foreach($res['Lines'] as $to_save){

                    $MaxHeight = $to_save['MaxHeight'];
                    $MinTop = $to_save['MinTop'];
                    $LineText = $to_save['LineText'];
                    $WordText = preg_replace('/-/', '', $to_save['Words'][0]['WordText']);
                    $Left = $to_save['Words'][0]['Left'];
                    $Top= $to_save['Words'][0]['Top'];
                    $Height = $to_save['Words'][0]['Height'];
                    $Width = $to_save['Words'][0]['Width'];
                    $datetime = date('Y-m-d H:i:s');

                        $to_save_field = ",'".$LineText."','".$WordText."','".$Left."','".$Top."','".$Height."','".$Width;
                      
                        $servername = "pj630578-001.privatesql";

$username = "usercabatho";

$password = "GUYfgy54fd541";

$dbname = "dbcabatho";

$port ='35735';

$link=mysqli_connect($servername, $username, $password, $dbname,$port);
                        if($link === false){
                            die("ERROR: Could not connect. " . mysqli_connect_error());
                        }

                        // foreach()
                        if($WordText != "" && $WordText !=" "){
                        $sql = "INSERT INTO `ocr_cabatho_data_active` (`id`,`id_save`, `data`,`file_name`, `datetime`,`LineText`,`WordText`,`pos_Left`,`pos_Top`,`pos_Height`,`Width`,`MaxHeight`,`MinTop`) VALUES (NULL,'".$target_file."','".str_replace("'","",$result_to_database)."','".$filename."','".$datetime."'".$to_save_field."','".$MaxHeight."','".$MinTop."')";
                        if(mysqli_query($link, $sql)){
                            //echo "Records inserted successfully.";
                        } else{
                            //echo "ERROR: Could not able to execute $sql. " . mysqli_error($link);
                        }
                    }
                }
            }
            
        }
        
        
        mysqli_close($link);
        if (!preg_match("/.gif/",$filename)){
        uploadToApiMoteur2($target_file,$filename);
        }else{
            synchronizeall($filename);
        }

    } else {
        header('HTTP/1.0 400 Forbidden');
        echo $response['ErrorMessage'];
    }
    } catch(Exception $err) {
        header('HTTP/1.0 403 Forbidden');
        echo $err->getMessage();
    }
}

// upload fichier moteur2 desactive

function uploadToApiMoteur2($target_file,$filename){
    
    require __DIR__ . '/vendor/autoload.php';
    $fileData = fopen($target_file, 'r');
    $client = new \GuzzleHttp\Client();
    try {
    $r = $client->request('POST', 'https://apipro2.ocr.space/parse/image',[
        'headers' => ['apiKey' => 'PKMXB8221888A'],
        'multipart' => [
            [
                'name' => 'file',
                'contents' => $fileData,
            ],
            [
                'name' => 'isOverlayRequired',
                'contents' => "true",
            ],
            [
                'name' => 'OCREngine',
                'contents' => "2",
            ]
        ]
    ], ['file' => $fileData]);
     $response =  json_decode($r->getBody(),true);
     $word = array_merge(range('a', 'z'), range('A', 'Z'));
                    shuffle($word);
                    $id_token = substr(implode($word), 0, 5);
    if($response['ErrorMessage'] == "") {
        $result_to_database = "";
        foreach($response['ParsedResults'] as $pareValue) {
            $result_to_database .= $pareValue['ParsedText']." ";
            foreach($pareValue as $res){
                foreach($res['Lines'] as $to_save){

                 
                    $MaxHeight = $to_save['MaxHeight'];
                    $MinTop = $to_save['MinTop'];
                    $LineText = $to_save['LineText'];
                    $WordText =preg_replace('/-/', '',$to_save['Words'][0]['WordText']);
                    $Left = $to_save['Words'][0]['Left'];
                    $Top= $to_save['Words'][0]['Top'];
                    $Height = $to_save['Words'][0]['Height'];
                    $Width = $to_save['Words'][0]['Width'];
                    $datetime = date('Y-m-d H:i:s');

                        $to_save_field = ",'".$LineText."','".$WordText."','".$Left."','".$Top."','".$Height."','".$Width;
                        $servername = "pj630578-001.privatesql";

$username = "usercabatho";

$password = "GUYfgy54fd541";

$dbname = "dbcabatho";

$port ='35735';

$link=mysqli_connect($servername, $username, $password, $dbname,$port);
                        if($link === false){
                            die("ERROR: Could not connect. " . mysqli_connect_error());
                        }
                        // foreach()
                        if($WordText !="" && $WordText != " "){
                         $sql = "INSERT INTO `ocr_cabatho_data_engine2` (`id`,`id_save`, `data`,`file_name`, `datetime`,`LineText`,`WordText`,`pos_Left`,`pos_Top`,`pos_Height`,`Width`,`MaxHeight`,`MinTop`) VALUES (NULL,'".$target_file."','".str_replace("'","",$result_to_database)."','".$filename."','".$datetime."'".$to_save_field."','".$MaxHeight."','".$MinTop."')";
                        if(mysqli_query($link, $sql)){
                           // echo "Records inserted successfully.";
                        } else{
                            //echo "ERROR: Could not able to execute $sql. " . mysqli_error($link);
                        }
                    }
                }
            }
            
        }
        
        
        mysqli_close($link);
     
        uploadToApimoteur2_active($target_file,$filename);
    } else {
        header('HTTP/1.0 400 Forbidden');
        echo $response['ErrorMessage'];
    }
    } catch(Exception $err) {
        header('HTTP/1.0 403 Forbidden');
        echo $err->getMessage();
    }
}

// Upload fichier Moteur2 active

function uploadToApimoteur2_active($target_file,$filename){
    require __DIR__ . '/vendor/autoload.php';

    $fileData = fopen($target_file, 'r');

    $client = new \GuzzleHttp\Client();
    try {
    $r = $client->request('POST', 'https://apipro2.ocr.space/parse/image',[
        'headers' => ['apiKey' => 'PKMXB8221888A'],
        'multipart' => [
            [
                'name' => 'file',
                'contents' => $fileData,
            ],
            [
                'name' => 'isOverlayRequired',
                'contents' => "true",
            ],
            [
                'name' => 'OCREngine',
                'contents' => "2",
            ],
            [
                'name' => 'scale',
                'contents' => "true",
            ]

        ]
    ], ['file' => $fileData]);
     $response =  json_decode($r->getBody(),true);
     
     $word = array_merge(range('a', 'z'), range('A', 'Z'));
                    shuffle($word);
                    $id_token = substr(implode($word), 0, 5);
    if($response['ErrorMessage'] == "") {
       
        $result_to_database = "";
        foreach($response['ParsedResults'] as $pareValue) {
            $result_to_database .= $pareValue['ParsedText']." ";
            foreach($pareValue as $res){
                foreach($res['Lines'] as $to_save){

                    $MaxHeight = $to_save['MaxHeight'];
                    $MinTop = $to_save['MinTop'];
                    $LineText = $to_save['LineText'];
                    $WordText = preg_replace('/-/', '', $to_save['Words'][0]['WordText']);
                    $Left = $to_save['Words'][0]['Left'];
                    $Top= $to_save['Words'][0]['Top'];
                    $Height = $to_save['Words'][0]['Height'];
                    $Width = $to_save['Words'][0]['Width'];
                    $datetime = date('Y-m-d H:i:s');

                        $to_save_field = ",'".$LineText."','".$WordText."','".$Left."','".$Top."','".$Height."','".$Width;
                        $servername = "pj630578-001.privatesql";

$username = "usercabatho";

$password = "GUYfgy54fd541";

$dbname = "dbcabatho";

$port ='35735';

$link=mysqli_connect($servername, $username, $password, $dbname,$port);
                        if($link === false){
                            die("ERROR: Could not connect. " . mysqli_connect_error());
                        }
                        // foreach()
                        if($WordText != "" && $WordText !=" "){
                         
                        $sql = "INSERT INTO `ocr_cabatho_data_engine_active` (`id`,`id_save`, `data`,`file_name`, `datetime`,`LineText`,`WordText`,`pos_Left`,`pos_Top`,`pos_Height`,`Width`,`MaxHeight`,`MinTop`) VALUES (NULL,'".$target_file."','".str_replace("'","",$result_to_database)."','".$filename."','".$datetime."'".$to_save_field."','".$MaxHeight."','".$MinTop."')";
                        if(mysqli_query($link, $sql)){
                           // echo "Records inserted successfully.";
                        } else{
                            //echo "ERROR: Could not able to execute $sql. " . mysqli_error($link);
                        }
                    }
                }
            }
            
        }
        
        
        mysqli_close($link);
        synchronizeall($filename);

    } else {
        header('HTTP/1.0 400 Forbidden');
        echo $response['ErrorMessage'];
    }
    } catch(Exception $err) {
        header('HTTP/1.0 403 Forbidden');
        echo $err->getMessage();
    }
}

//upload fichier synchonisation
function insert1($filename)
{
   
    $servername = "pj630578-001.privatesql";

$username = "usercabatho";

$password = "GUYfgy54fd541";

$dbname = "dbcabatho";

$port ='35735';

$con=mysqli_connect($servername, $username, $password, $dbname,$port);

    if (mysqli_connect_errno())
    {
        echo "Failed to connect to MySQL: " . mysqli_connect_error();
    }

    $file_name = $filename;
    if (isset($file_name) && $file_name != null){
        $query2 = "SELECT * FROM ocr_cabatho_data WHERE file_name = '".$file_name."'";

        $result2 = $con->query($query2);
            while($row2 = $result2->fetch_assoc()) {

                    $MaxHeight = $row2['MaxHeight'];
                    $MinTop = $row2['MinTop'];
                    $LineText = $row2['LineText'];
                    $WordText =preg_replace('/-/', '',$row2['WordText']);
                    $Left = $row2['pos_Left'];
                    $Top= $row2['pos_Top'];
                    $Height = $row2['pos_Height'];
                    $Width = $row2['Width'];
                    $datetime = date('Y-m-d H:i:s');
                    $to_save_field = ",'".$LineText."','".$WordText."','".$Left."','".$Top."','".$Height."','".$Width;
                    $result_to_database = $row2['data'];
                    $filename =$row2['file_name'];
                    $target_file =$row2['id_save'];
                    if($WordText !="" && $WordText !=" "){
                    $query_insert = "INSERT INTO `ocr_cabatho_data_sync_ok` (`id`,`id_save`, `data`,`file_name`, `datetime`,`LineText`,`WordText`,`pos_Left`,`pos_Top`,`pos_Height`,`Width`,`MaxHeight`,`MinTop`) VALUES (NULL,'".$target_file."','".str_replace("'","",$result_to_database)."','".$filename."','".$datetime."'".$to_save_field."','".$MaxHeight."','".$MinTop."')";
                    if(mysqli_query($con, $query_insert)){
                        echo 1;
                    }else{
                        echo 0;
                    }
                }

    }

    insert2($filename);

    if(!preg_match("/.gif/",$filename)){
    insert3($filename);
    insert4($filename);
    }
}
}
function insert2($file_name)
{
    $servername = "pj630578-001.privatesql";

$username = "usercabatho";

$password = "GUYfgy54fd541";

$dbname = "dbcabatho";

$port ='35735';

$con=mysqli_connect($servername, $username, $password, $dbname,$port);

    if (mysqli_connect_errno())
    {
        echo "Failed to connect to MySQL: " . mysqli_connect_error();
    }

    $array_check2 = [];
    $query3 = "SELECT * FROM ocr_cabatho_data_sync_ok WHERE file_name = '".$file_name."'";
    $result3 = $con->query($query3);

    while($row3 = $result3->fetch_assoc()) {
         array_push($array_check2,$row3['WordText']);
    }

    $query5 = "SELECT * FROM ocr_cabatho_data_active WHERE file_name = '".$file_name."'";

    $result5 = $con->query($query5);

    while($row4 = $result5->fetch_assoc()) {
        if (in_array($row4['WordText'],$array_check2)){
            echo 'c';
        }else{
            $MaxHeight2 = $row4['MaxHeight'];
            $MinTop2 = $row4['MinTop'];
            $LineText2 = $row4['LineText'];
            $WordText2 =preg_replace('/-/', '',$row4['WordText']);
            $Left2 = $row4['pos_Left'];
            $Top2= $row4['pos_Top'];
            $Height2 = $row4['pos_Height'];
            $Width2 = $row4['Width'];
            $datetime2 = date('Y-m-d H:i:s');
            $to_save_field2 = ",'".$LineText2."','".$WordText2."','".$Left2."','".$Top2."','".$Height2."','".$Width2;
            $result_to_database2 = $row4['data'];
            $filename2 =$row4['file_name'];
            $target_file2 =$row4['id_save'];
            if($WordText2 !="" && $WordText2 !=" "){
            $query_insert2 = "INSERT INTO `ocr_cabatho_data_sync_ok` (`id`,`id_save`, `data`,`file_name`, `datetime`,`LineText`,`WordText`,`pos_Left`,`pos_Top`,`pos_Height`,`Width`,`MaxHeight`,`MinTop`) VALUES (NULL,'".$target_file2."','".str_replace("'","",$result_to_database2)."','".$filename2."','".$datetime2."'".$to_save_field2."','".$MaxHeight2."','".$MinTop2."')";
            if(mysqli_query($con, $query_insert2)){
                echo 'ok moteur 1 active';
            }else{
                echo 'ereur moteur 1 active';
            }
        }

        }
    }
}

function insert3($filename)
{

    $servername = "pj630578-001.privatesql";

$username = "usercabatho";

$password = "GUYfgy54fd541";

$dbname = "dbcabatho";

$port ='35735';

$con=mysqli_connect($servername, $username, $password, $dbname,$port);

    if (mysqli_connect_errno())
    {
        echo "Failed to connect to MySQL: " . mysqli_connect_error();
    }

    $file_name = $filename;
  
    $array_check3 = [];
    $query3 = "SELECT * FROM ocr_cabatho_data_sync_ok WHERE file_name = '".$file_name."'";
    $result3 = $con->query($query3);
    while($row3 = $result3->fetch_assoc()) {
        array_push($array_check3,$row3['WordText']);
    }
    $query5 = "SELECT * FROM ocr_cabatho_data_engine2 WHERE file_name = '".$file_name."'";
    $result5 = $con->query($query5);
    while($row4 = $result5->fetch_assoc()) {
        if (in_array($row4['WordText'],$array_check3)){
            echo 'c';
        }else{
            $MaxHeight2 = $row4['MaxHeight'];
            $MinTop2 = $row4['MinTop'];
            $LineText2 = $row4['LineText'];
            $WordText2 =preg_replace('/-/', '',$row4['WordText']);
            $Left2 = $row4['pos_Left'];
            $Top2= $row4['pos_Top'];
            $Height2 = $row4['pos_Height'];
            $Width2 = $row4['Width'];
            $datetime2 = date('Y-m-d H:i:s');
            $to_save_field2 = ",'".$LineText2."','".$WordText2."','".$Left2."','".$Top2."','".$Height2."','".$Width2;
            $result_to_database2 = $row4['data'];
            $filename2 =$row4['file_name'];
            $target_file2 =$row4['id_save'];
            if($WordText2 !="" && $WordText2 !=" "){
            $query_insert3 = "INSERT INTO `ocr_cabatho_data_sync_ok` (`id`,`id_save`, `data`,`file_name`, `datetime`,`LineText`,`WordText`,`pos_Left`,`pos_Top`,`pos_Height`,`Width`,`MaxHeight`,`MinTop`) VALUES (NULL,'".$target_file2."','".str_replace("'","",$result_to_database2)."','".$filename2."','".$datetime2."'".$to_save_field2."','".$MaxHeight2."','".$MinTop2."')";
            if(mysqli_query($con, $query_insert3)){
                echo 'ok moteur 2 inactive';
            }else{
                echo 'erreur moteur 2 inactive';
            }
        }
        }
    }
}

function insert4($filename)
{
    $servername = "pj630578-001.privatesql";

$username = "usercabatho";

$password = "GUYfgy54fd541";

$dbname = "dbcabatho";

$port ='35735';

$con=mysqli_connect($servername, $username, $password, $dbname,$port);

    if (mysqli_connect_errno())
    {
        echo "Failed to connect to MySQL: " . mysqli_connect_error();
    }

    $file_name = $filename;
    $array_check4 = [];
    $query3 = "SELECT * FROM ocr_cabatho_data_sync_ok WHERE file_name = '".$file_name."'";
    $result3 = $con->query($query3);
    while($row3 = $result3->fetch_assoc()) {
        array_push($array_check4,$row3['WordText']);
    }
    $query5 = "SELECT * FROM ocr_cabatho_data_engine_active WHERE file_name = '".$file_name."'";
    $result5 = $con->query($query5);
    while($row4 = $result5->fetch_assoc()) {
        if (in_array($row4['WordText'],$array_check4)){
            echo 'dededededede';
        }else{
            $MaxHeight2 = $row4['MaxHeight'];
            $MinTop2 = $row4['MinTop'];
            $LineText2 = $row4['LineText'];
            $WordText2 =preg_replace('/-/', '',$row4['WordText']);
            $Left2 = $row4['pos_Left'];
            $Top2= $row4['pos_Top'];
            $Height2 = $row4['pos_Height'];
            $Width2 = $row4['Width'];
            $datetime2 = date('Y-m-d H:i:s');
            $to_save_field2 = ",'".$LineText2."','".$WordText2."','".$Left2."','".$Top2."','".$Height2."','".$Width2;
            $result_to_database2 = $row4['data'];
            $filename2 =$row4['file_name'];
            $target_file2 =$row4['id_save'];
            if($WordText2 !="" && $WordText2 !=" "){
            $query_insert4 = "INSERT INTO `ocr_cabatho_data_sync_ok` (`id`,`id_save`, `data`,`file_name`, `datetime`,`LineText`,`WordText`,`pos_Left`,`pos_Top`,`pos_Height`,`Width`,`MaxHeight`,`MinTop`) VALUES (NULL,'".$target_file2."','".str_replace("'","",$result_to_database2)."','".$filename2."','".$datetime2."'".$to_save_field2."','".$MaxHeight2."','".$MinTop2."')";
            if(mysqli_query($con, $query_insert4)){
                echo 'ok moteur 2 active';
            }else{
                echo 'erreur moteur 2 active';
            }
        }
        }
    }
}


function synchronizeall($filename){
    insert1($filename);


    header('Refresh:1;URL=http://www.magento1.randevteam.com/vue_eclate/');
}





function generateRandomString($length = 10) {
    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $charactersLength = strlen($characters);
    $randomString = '';
    for ($i = 0; $i < $length; $i++) {
        $randomString .= $characters[rand(0, $charactersLength - 1)];
    }
    return $randomString;
}

?>