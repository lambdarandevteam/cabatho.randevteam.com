{*
*  @author    Miguel Costa for emotionLoop
*  @copyright emotionLoop
*}
<div class="" id="_desktop_top_menu" style="width: 32%" >
    <div class="menu js-top-menu position-static hidden-sm-down" id="_desktop_top_menu_princip">
        <ul class="top-menu">
                <li class="category" id="menu_univers">
                    <a class="dropdown-item  category-5" href="javascript:void(0)">
                        <span class="float-xs-right hidden-md-up">
                            <span class="navbar-toggler collapse-icons">
                              <i class="material-icons add">&#xE313;</i>
                              <i class="material-icons remove">&#xE316;</i>
                            </span>
                        </span>
                           NOS UNIVERS
                    </a>
                    <div class="popover sub-menu js-sub-menu collapse" id="">

                    </div>
                </li>

            <li class="manufacturer" id="menu_marques">
                <a class="dropdown-item  category-5" href="javascript:void(0)">
                        <span class="float-xs-right hidden-md-up">
                            <span class="navbar-toggler collapse-icons">
                              <i class="material-icons add">&#xE313;</i>
                              <i class="material-icons remove">&#xE316;</i>
                            </span>
                        </span>
                    NOS MARQUES
                </a>
                <div class="popover sub-menu js-sub-menu collapse" id="">

                </div>
            </li>

            <li class="category" id="menu_nos_produits">
                <a class="dropdown-item  category-5" href="javascript:void(0)">
                        <span class="float-xs-right hidden-md-up">
                            <span class="navbar-toggler collapse-icons">
                              <i class="material-icons add">&#xE313;</i>
                              <i class="material-icons remove">&#xE316;</i>
                            </span>
                        </span>
                    NOS PRODUITS
                </a>
                <div class="popover sub-menu js-sub-menu collapse" id="">

                </div>
            </li>

        </ul>
        <div class="clearfix"></div>
    </div>
</div>

<br>

<div class="col-lg-12 sub-menu p-0" id="cbt_mainmenu_categ" style="display: none; position: absolute; top: 75px!important; z-index: 100;background-color: white">
    <div class="menu js-top-menu position-static hidden-sm-down" style="">
        <ul class="top-menu">
            {foreach from=$category item=categ}
                <li class="category col-lg-4 p-0" id="id_{$categ['id_categ']}">
                    <a
                            class="dropdown-item dropdown-submenu category-5"
                            href="/{$categ['categ_url']}"
                    >
                            <span class="float-xs-right hidden-md-up">
                    <span class="navbar-toggler collapse-icons">
                      <i class="material-icons add">&#xE313;</i>
                      <i class="material-icons remove">&#xE316;</i>
                    </span>
                  </span>
                        {*{$categ|@var_dump}*}
                        {if $categ['id_categ'] == "4"}
                            <img class="img-fluid" src="/themes/classic/assets/img/1image.png" width="100" height="80">
                            <span class="univ_{$categ['categ_name']|replace:' ':'_'|replace:'&':'et'}">{$categ['categ_name']}</span>
                        {elseif $categ['id_categ'] == "5"}
                            <img class="img-fluid" src="/themes/classic/assets/img/2image.png" width="100" height="80">
                            <span class="univ_{$categ['categ_name']|replace:' ':'_'|replace:'&':'et'}">{$categ['categ_name']}</span>
                        {elseif $categ['id_categ'] == "10"}
                            <img class="img-fluid" src="/themes/classic/assets/img/3image.png" width="100" height="80">
                            <span class="univ_{$categ['categ_name']|replace:' ':'_'|replace:'&':'et'}">{$categ['categ_name']}</span>
                        {else}
                            {$categ['categ_name']}
                        {/if}

                    </a>
                        <div class="popover sub-menu js-sub-menu collapse" id="">

                        </div>
                </li>
            {/foreach}
        </ul>
        <div class="clearfix"></div>
    </div>
</div>

<div class="col-lg-12 sub-menu" id="cbt_mainmenu_marque" style="display: none; position: absolute; top: 75px!important; z-index: 100;background-color: white">
    <div class="menu js-top-menu position-static hidden-sm-down" style="">
        <ul class="top-menu" style="display: flex">
            {assign var=arr value='A'|range:'Z'}
            {foreach from=$arr item=item}
                <div class="brand_separate{$item}" style="display: none">
                <span class="letter_brand_tittle_{$item}" style="display: none">{$item}</span>
                {foreach from=$marques item=marq}
                    {*{$marq['marque_name'][0]|var_dump}*}
                    {if $marq['marque_name'][0]|upper ==$item}
                    <style>
                        .brand_separate{$item}{
                            display: block!important;
                        }
                        .letter_brand_tittle_{$item}{
                            color: #36b0d2;
                            text-decoration: none;
                            font-family: Muli-ExtraBold !important;
                            font-size: 18px;
                            float: left;
                            text-transform: uppercase;
                            margin-bottom: 0px !important;
                            width: 100%;
                            display: block!important;
                            padding-top: 10px;
                        }
                    </style>
                    <li class="manufacturer" id="">
                        <a
                                class="dropdown-item dropdown-submenu category-5"
                                href="{$marq['marque_url']}"
                        >
                            <span class="float-xs-right hidden-md-up">
                    <span class="navbar-toggler collapse-icons">
                      <i class="material-icons add">&#xE313;</i>
                      <i class="material-icons remove">&#xE316;</i>
                    </span>
                  </span>

                            {$marq['marque_name']}

                        </a>
                        <div class="popover sub-menu js-sub-menu collapse" id="">

                        </div>
                    </li>
                    {/if}
                {/foreach}
                </div>
            {/foreach}
        </ul>
        <div class="clearfix"></div>
    </div>
</div>

<div class="col-lg-12 sub-menu" id="cbt_mainmenu_nos_produit" style="display: none; position: absolute; top: 75px!important; z-index: 100;background-color: white">
    <div class="menu js-top-menu position-static hidden-sm-down" style="">
        <ul class="top-menu" style="display: flex">
            {assign var=arr value='A'|range:'Z'}
            {foreach from=$arr item=item}
            <div class="prod_separate{$item}" style="display: none;">
            <span class="letter_prod_tittle_{$item}" style="display: none">{$item}</span>
            {foreach from=$produits item=prds}
                {if $prds['nom_feature'][0]|upper ==$item}
                <style>
                    .prod_separate{$item}{
                        display: block!important;
                    }
                    .letter_prod_tittle_{$item}{
                        color: #36b0d2;
                        text-decoration: none;
                        font-family: Muli-ExtraBold !important;
                        font-size: 18px;
                        float: left;
                        text-transform: uppercase;
                        margin-bottom: 0px !important;
                        width: 100%;
                        display: block!important;
                        padding-top: 10px;
                    }
                </style>
                <li class="category" id="">
                    <a
                            class="dropdown-item dropdown-submenu category-5"
                            href="/3-nos-univers?q=Nos+produits-{$prds['nom_feature']}"
                    >
                            <span class="float-xs-right hidden-md-up">
                    <span class="navbar-toggler collapse-icons">
                      <i class="material-icons add">&#xE313;</i>
                      <i class="material-icons remove">&#xE316;</i>
                    </span>
                  </span>

                            {$prds['nom_feature']}

                    </a>
                    <div class="popover sub-menu js-sub-menu collapse" id="">

                    </div>
                </li>
                {/if}
            {/foreach}
            </div>
            {/foreach}
        </ul>
        <div class="clearfix"></div>
    </div>
</div>