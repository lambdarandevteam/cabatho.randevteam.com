{**
 * 2007-2019 PrestaShop and Contributors
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License 3.0 (AFL-3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * https://opensource.org/licenses/AFL-3.0
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to https://www.prestashop.com for more information.
 *
 * @author    PrestaShop SA <contact@prestashop.com>
 * @copyright 2007-2019 PrestaShop SA and Contributors
 * @license   https://opensource.org/licenses/AFL-3.0 Academic Free License 3.0 (AFL-3.0)
 * International Registered Trademark & Property of PrestaShop SA
 *}

<ul id="list_brand_ul">
  {foreach from=$brands item=brand name=brand_list}
    {if $smarty.foreach.brand_list.iteration <= $text_list_nb}
      <li style="{if $page.page_name !='index' && $page.page_name !='product'}display :none!important;{/if}" id="brand_{$brand['id_manufacturer']}" class="facet-label d-inline brandese">
        <a href="{$brand['link']}" title="{$brand['name']}">
            <img src="/img/m/{$brand['id_manufacturer']}.jpg" alt="{$brand['name']}"/>
        </a>
      </li>
    {/if}
  {/foreach}
</ul>
<div class="container cab-brand-logo">
  <div class="row">
    <div class="col-lg-12">
      <div class="row">
        <div class="col-lg-4"></div>
        <div class="col-lg-4">
          <div class="btn-marque" onclick="click_btn_brand()">
            <a id="btn_brand" class="text">VOIR TOUTES NOS MARQUES
              <span class="icon_down">
                <img class="img-fluid image_down" src="{$urls.img_url}icon_down.png" width="25" height="20">
              </span>
            </a>
          </div>
        </div>
        <div class="col-lg-4"></div>
      </div>
    </div>
  </div>
</div>
<script type="text/javascript">
    function click_btn_brand() {

        if (document.getElementById('btn_brand').className === "text"){
            document.getElementById('list_brand_ul').id = 'list_brand_ul1';
            document.getElementById("btn_brand").classList.remove('text');
            document.getElementById("btn_brand").classList.add('text1');
        }
        else{
            document.getElementById('list_brand_ul1').id = 'list_brand_ul';
            document.getElementById("btn_brand").classList.remove('text1');
            document.getElementById("btn_brand").classList.add('text');
        }
    }
</script>
