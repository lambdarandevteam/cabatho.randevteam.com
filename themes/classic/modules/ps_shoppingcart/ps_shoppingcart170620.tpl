{**
 * 2007-2019 PrestaShop and Contributors
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License 3.0 (AFL-3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * https://opensource.org/licenses/AFL-3.0
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to https://www.prestashop.com for more information.
 *
 * @author    PrestaShop SA <contact@prestashop.com>
 * @copyright 2007-2019 PrestaShop SA and Contributors
 * @license   https://opensource.org/licenses/AFL-3.0 Academic Free License 3.0 (AFL-3.0)
 * International Registered Trademark & Property of PrestaShop SA
 *}
<div class="cbt_shoppingcart">
    <div id="_desktop_cart">
        {*<div class="blockcart cart-preview {if $cart.products_count > 0}active{else}inactive{/if}" data-refresh-url="{$refresh_url}">*}
            {*<div class="header">*}
                {*{if $cart.products_count > 0}*}
                {*<a rel="nofollow" href="{$cart_url}">*}
                    {*{/if}*}
                    {*<i class="material-icons shopping-cart">shopping_cart</i>*}
                    {*<img class="img-fluid" src="{$urls.img_url}cart.png">*}
                    {*<span class="hidden-sm-down">{l s='Cart' d='Shop.Theme.Checkout'}</span>*}
                    {*<span class="cart-products-count">({$cart.products_count})</span>*}
                    {*{if $cart.products_count > 0}*}
                {*</a>*}
                {*{/if}*}
            {*</div>*}
        {*</div>*}
        <div class="blockcart cart-preview {if $cart.products_count > 0}active{else}inactive{/if}" data-refresh-url="{$refresh_url}">
            <div class="dropdown">
                <button class="dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <img class="img-fluid" src="{$urls.img_url}cart.png">
                    <span class="hidden-sm-down">{l s='Mon panier' d='Shop.Theme.Checkout'}</span>
                </button>
                <div class="dropdown-menu cart_content" aria-labelledby="dropdownMenuButton">
                    <div class="toolbar-dropdown">
                        {if $cart.products_count > 0}
                        {foreach from=$cart.products item=product}
                            {include 'module:ps_shoppingcart/ps_shoppingcart-product-line.tpl' product=$product}
                        {/foreach}
                        {*<div class="toolbar-dropdown-group">*}
                        {*{foreach from=$cart.subtotals item="subtotal"}*}
                        {*<div class="col-xs-6">{$subtotal.label}</span></div>*}
                        {*<div class="col-xs-6 text-right">{$subtotal.value}</div>*}
                        {*{/foreach}*}
                        {*<div class="col-xs-6">{$cart.totals.total.label}</div>*}
                        {*<div class="col-xs-6 text-right">{$cart.totals.total.value}</div>*}
                        {*</div>*}
                        <div class="toolbar-dropdown-group">
                            <div class="col-xs-6">{l s='Total' d='Shop.Theme.Actions'}</span></div>
                            <div class="col-xs-6 text-right">{$cart.subtotals.products.value}</div>
                        </div>
                    </div>
                    <div class="toolbar-dropdown-total">
                        <a class="btn-checkout btn btn-primary" href="{$urls.pages.order}">
                            {*{l s='Commander' d='Shop.Theme.Actions'}*}
                            Commander
                        </a>
                        <a class="btn-view btn btn-secondary" href="{$cart_url}">
                            {*{l s='Voir panier' d='Shop.Theme.Actions'}*}
                            Voir panier
                        </a>
                    </div>
                    {else}
                        <p class="text-center mt-5 pt-5 pb-5 mb-5">
                            {*{l s='Votre panier est vide' d='Shop.Theme.Actions'}*}
                            Votre panier est vide
                        </p>
                    </div>
                    {/if}
                </div>
            </div>
        </div>
    </div>
</div>

