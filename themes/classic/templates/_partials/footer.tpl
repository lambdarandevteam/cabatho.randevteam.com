{**
 * 2007-2019 PrestaShop and Contributors
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License 3.0 (AFL-3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * https://opensource.org/licenses/AFL-3.0
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to https://www.prestashop.com for more information.
 *
 * @author    PrestaShop SA <contact@prestashop.com>
 * @copyright 2007-2019 PrestaShop SA and Contributors
 * @license   https://opensource.org/licenses/AFL-3.0 Academic Free License 3.0 (AFL-3.0)
 * International Registered Trademark & Property of PrestaShop SA
 *}
  {if $page.page_name == 'product'}
    {if isset($product->id_category_default) && $product->id_category_default == "4"}﻿﻿
      {block name='DisplayAfterlistproduct'}
        {hook h='DisplayAfterlistproduct'}
      {/block}
    {elseif isset($product->id_category_default) && $product->id_category_default == "5"}
      {block name='DisplayAfterlistproduct2'}
        {hook h='DisplayAfterlistproduct2'}
      {/block}
    {elseif isset($product->id_category_default) && $product->id_category_default == "10"}
      {block name='DisplayAfterlistproduct3'}
        {hook h='DisplayAfterlistproduct3'}
      {/block}
    {/if}﻿﻿
  {/if}
  {if $page.page_name == 'category'}
    {if isset($category.id) && $category.id =="4"}﻿﻿
      {block name='DisplayAfterlistproduct'}
        {hook h='DisplayAfterlistproduct'}
      {/block}
    {elseif isset($category.id) && $category.id =="5"}
      {block name='DisplayAfterlistproduct2'}
        {hook h='DisplayAfterlistproduct2'}
      {/block}
    {elseif isset($category.id) && $category.id =="10"}
      {block name='DisplayAfterlistproduct3'}
        {hook h='DisplayAfterlistproduct3'}
      {/block}
    {/if}﻿﻿
  {/if}
<div class="container">
  <div class="row">
    {block name='hook_footer_before'}
      {hook h='displayFooterBefore'}
    {/block}
  </div>
</div>
<div class="footer-container">
  <div class="container">
    <div class="row">
      {block name='hook_footer'}
        {hook h='displayFooter'}
      {/block}
    </div>
    <div class="row">
      {block name='hook_footer_after'}
        {hook h='displayFooterAfter'}
      {/block}
    </div>
  </div>
</div>
