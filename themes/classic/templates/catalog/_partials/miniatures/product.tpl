{**
 * 2007-2019 PrestaShop and Contributors
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License 3.0 (AFL-3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * https://opensource.org/licenses/AFL-3.0
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to https://www.prestashop.com for more information.
 *
 * @author    PrestaShop SA <contact@prestashop.com>
 * @copyright 2007-2019 PrestaShop SA and Contributors
 * @license   https://opensource.org/licenses/AFL-3.0 Academic Free License 3.0 (AFL-3.0)
 * International Registered Trademark & Property of PrestaShop SA
 *}
{if isset($product.price) && $product.price != null && $product.price != ''}
{block name='product_miniature_item'}
  <article class="product-miniature js-product-miniature" data-id-product="{$product.id_product}" data-id-product-attribute="{$product.id_product_attribute}" itemscope itemtype="http://schema.org/Product">
    <div class="thumbnail-container">
      {block name='product_thumbnail'}
        {if $product.cover}
          <a href="{$product.url}" class="thumbnail product-thumbnail">
            <img
              src="{$product.cover.bySize.home_default.url}"
              alt="{if !empty($product.cover.legend)}{$product.cover.legend}{else}{$product.name|truncate:30:'...'}{/if}"
              data-full-size-image-url="{$product.cover.large.url}"
            />
          </a>
        {else}
          <a href="{$product.url}" class="thumbnail product-thumbnail">
            <img src="{$urls.no_picture_image.bySize.home_default.url}" />
          </a>
        {/if}
      {/block}
      <div class="product-description">
          <div class="container">
              {foreach from=$product.features item=feature}
                  {if isset($feature.value) && $feature.value|escape:'html':'UTF-8' === "vue éclatée"}
                      <div class="vue_eclat">
                          <div class="row">
                              <div class="content_type_vue d-flex">
                                  <span class="vue">Voir la</span> <span class="eclat">Vue éclatée</span>
                              </div>
                              <div class="image_type_vue d-flex">
                                  <img class="img-fluid" src="{$urls.img_url}vue_eclat.png" width="30" height="auto">
                              </div>
                          </div>
                      </div>
                  {elseif isset($feature.value) && $feature.value|escape:'html':'UTF-8' === "vue sans éclat"}
                      <div class="vue_sans_eclat">
                          <div class="row">
                              <div class="content_type_vue d-flex">
                                  <input type="text" class="input_cherche">
                              </div>
                              <div class="image_type_vue d-flex">
                                  <img class="img-fluid" src="{$urls.img_url}recherche.png" width="30" height="auto">
                              </div>
                          </div>
                      </div>
                  {/if}
              {/foreach}
          </div>
          <div class="container">
              <div class="row">
                  <div class="class_title_product_item">
                      {block name='product_name'}
                          {if $page.page_name == 'index'}
                              <h3 class="h3 product-title" itemprop="name"><a href="{$product.url}">{$product.name|truncate:20:'...'}</a></h3>
                          {else}
                              <h2 class="h3 product-title" itemprop="name"><a href="{$product.url}">{$product.name|truncate:20:'...'}</a></h2>
                              {foreach from=$product.features item=feature}
                                  {if $feature.id_feature == 4}
                                      <span class="modele_product_categ">{$feature.value}</span>
                                  {/if}
                              {/foreach}
                          {/if}
                      {/block}
                  </div>
                  {if isset($product.id_manufacturer)}
                      <div class="manufacturer_image_item">
                          <div class="product-brand text-center">
                              {block name='product_brand'}
                                  <a href="{$link->getManufacturerLink($product.id_manufacturer)}" title="manufacturer-{$product.id_manufacturer}">
                                      <img class="img-fluid" src="{$link->getManufacturerImageLink($product.id_manufacturer, 'small_default')}" alt="image-{$product.id_manufacturer}" width="60" height="50">
                                  </a>
                                  <script type="text/javascript">
                                      document.getElementById('brand_{$product.id_manufacturer}').style.display = 'flex';
                                  </script>
                              {/block}
                          </div>
                      </div>
                  {/if}
              </div>
          </div>

        {*{block name='product_price_and_shipping'}*}
          {*{if $product.show_price}*}
            {*<div class="product-price-and-shipping">*}
              {*{if $product.has_discount}*}
                {*{hook h='displayProductPriceBlock' product=$product type="old_price"}*}

                {*<span class="sr-only">{l s='Regular price' d='Shop.Theme.Catalog'}</span>*}
                {*<span class="regular-price">{$product.regular_price}</span>*}
                {*{if $product.discount_type === 'percentage'}*}
                  {*<span class="discount-percentage discount-product">{$product.discount_percentage}</span>*}
                {*{elseif $product.discount_type === 'amount'}*}
                  {*<span class="discount-amount discount-product">{$product.discount_amount_to_display}</span>*}
                {*{/if}*}
              {*{/if}*}

              {*{hook h='displayProductPriceBlock' product=$product type="before_price"}*}

              {*<span class="sr-only">{l s='Price' d='Shop.Theme.Catalog'}</span>*}
              {*<span itemprop="price" class="price">{$product.price}</span>*}

              {*{hook h='displayProductPriceBlock' product=$product type='unit_price'}*}

              {*{hook h='displayProductPriceBlock' product=$product type='weight'}*}
            {*</div>*}
          {*{/if}*}
        {*{/block}*}

        {*{block name='product_reviews'}*}
          {*{hook h='displayProductListReviews' product=$product}*}
        {*{/block}*}
      </div>

      <!-- @todo: use include file='catalog/_partials/product-flags.tpl'} -->
      {*{block name='product_flags'}*}
        {*<ul class="product-flags">*}
          {*{foreach from=$product.flags item=flag}*}
            {*<li class="product-flag {$flag.type}">{$flag.label}</li>*}
          {*{/foreach}*}
        {*</ul>*}
      {*{/block}*}

      {*<div class="highlighted-informations{if !$product.main_variants} no-variants{/if} hidden-sm-down">*}
        {*{block name='quick_view'}*}
          {*<a class="quick-view" href="#" data-link-action="quickview">*}
            {*<i class="material-icons search">&#xE8B6;</i> {l s='Quick view' d='Shop.Theme.Actions'}*}
          {*</a>*}
        {*{/block}*}

        {*{block name='product_variants'}*}
          {*{if $product.main_variants}*}
            {*{include file='catalog/_partials/variant-links.tpl' variants=$product.main_variants}*}
          {*{/if}*}
        {*{/block}*}
      {*</div>*}
    </div>
  </article>
{/block}
{/if}