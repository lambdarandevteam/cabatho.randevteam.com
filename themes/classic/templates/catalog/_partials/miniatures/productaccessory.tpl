{**
 * 2007-2019 PrestaShop and Contributors
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License 3.0 (AFL-3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * https://opensource.org/licenses/AFL-3.0
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to https://www.prestashop.com for more information.
 *
 * @author    PrestaShop SA <contact@prestashop.com>
 * @copyright 2007-2019 PrestaShop SA and Contributors
 * @license   https://opensource.org/licenses/AFL-3.0 Academic Free License 3.0 (AFL-3.0)
 * International Registered Trademark & Property of PrestaShop SA
 *}
{if isset($product.price) && $product.price != null && $product.price != ''}
{block name='product_miniature_item'}
  <article class="product-miniature js-product-miniature" data-id-product="{$product.id_product}" data-id-product-attribute="{$product.id_product_attribute}" itemscope itemtype="http://schema.org/Product">
    <div class="thumbnail-container">
      {block name='product_thumbnail'}
        {if $product.cover}
          <a href="{$product.url}" class="thumbnail product-thumbnail" id="img_container_{$product.id_product}">
            <img
              src="{$product.cover.bySize.home_default.url}"
              alt="{if !empty($product.cover.legend)}{$product.cover.legend}{else}{$product.name|truncate:30:'...'}{/if}"
              data-full-size-image-url="{$product.cover.large.url}"
            />
          </a>
        {else}
          <a href="{$product.url}" class="thumbnail product-thumbnail">
            <img src="{$urls.no_picture_image.bySize.home_default.url}" />
          </a>
        {/if}
      {/block}
        <script src='https://cdnjs.cloudflare.com/ajax/libs/jquery/1.10.2/jquery.min.js'></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-migrate/3.3.1/jquery-migrate.min.js" integrity="sha512-wDH73bv6rW6O6ev5DGYexNboWMzBoY+1TEAx5Q/sdbqN2MB2cNTG9Ge/qv3c1QNvuiAuETsKJnnHH2UDJGmmAQ==" crossorigin="anonymous"></script>
        <script>
            {literal}
            $(document).ready(function(){
                $('#img_container_{/literal}{$product.id_product}{literal}').zoom();
            });
            {/literal}
        </script>
        <div class="product-description" style="height: 100px;">
          <div class="container">
              <div class="vue_eclat">
                  <div class="row">
                      <div class="content_type_vue d-flex">
                          <span class="eclat">Zoomer sur la piéce</span>
                      </div>
                      <div class="image_type_vue d-flex">
                          <img class="img-fluid" src="{$urls.img_url}recherche.png" width="30" height="auto">
                      </div>
                  </div>
              </div>
          </div>
          <div class="container">
              <div class="row">
                  <div class="class_title_product_item">
                      {block name='product_name'}
                          {if $page.page_name == 'index'}
                              <h3 class="h3 product-title" itemprop="name"><a href="{$product.url}">{$product.name|truncate:20:'...'}</a></h3>
                          {else}
                              <h2 class="h3 product-title" itemprop="name" style="padding-left: 5px;"><a href="{$product.url}">{$product.name|truncate:20:'...'}</a></h2>
                              {foreach from=$product.features item=feature}
                                  {if $feature.id_feature == 4}
                                      <span class="modele_product_categ" style="padding-left: 5px;">{$feature.value}</span>
                                  {/if}
                              {/foreach}
                          {/if}
                      {/block}
                  </div>
                  {if isset($product.id_manufacturer)}
                      <div class="manufacturer_image_item">
                          <div class="product-brand text-center">
                              {block name='product_brand'}
                                  <a href="{$link->getManufacturerLink($product.id_manufacturer)}" title="manufacturer-{$product.id_manufacturer}">
                                      <img class="img-fluid" src="{$link->getManufacturerImageLink($product.id_manufacturer, 'small_default')}" alt="image-{$product.id_manufacturer}" width="60" height="50">
                                  </a>
                                  <script type="text/javascript">
                                      document.getElementById('brand_{$product.id_manufacturer}').style.display = 'flex';
                                  </script>
                              {/block}
                          </div>
                      </div>
                  {/if}
              </div>
          </div>

      </div>
    </div>
  </article>
{/block}
{/if}