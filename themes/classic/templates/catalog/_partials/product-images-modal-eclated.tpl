{**
 * 2007-2019 PrestaShop and Contributors
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License 3.0 (AFL-3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * https://opensource.org/licenses/AFL-3.0
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to https://www.prestashop.com for more information.
 *
 * @author    PrestaShop SA <contact@prestashop.com>
 * @copyright 2007-2019 PrestaShop SA and Contributors
 * @license   https://opensource.org/licenses/AFL-3.0 Academic Free License 3.0 (AFL-3.0)
 * International Registered Trademark & Property of PrestaShop SA
 *}
<div class="modal fade js-product-images-modal" id="product-modal">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-body">
        {assign var=imagesCount value=$product.images|count}
        <figure id="modal_txtxx">
            {foreach from=$product.grouped_features item=features}
                <script type="text/javascript">
                    // window.onload = function() {
                        if (window.jQuery) {
                            var datas = "file_name={$features.value}.jpg";
                            var url = "{$urls.base_url}ocr/request.php";
                            $.ajax({
                                url:url,
                                method:"POST",
                                data:datas,
                                success:function(data)
                                {
                                    var data_parsed2 = JSON.parse(data);
                                    for(let e=0;e<data_parsed2.length;e++){
                                        // if (parseInt(data_parsed2[e]["pos_Left"]) < 580){
                                            var to_show2 = "<span id='span_"+data_parsed2[e]["id"]+"' style='color:red;position:absolute;left:"+data_parsed2[e]["pos_Left"]+"px;top:"+data_parsed2[e]["pos_Top"]+"px'>"+data_parsed2[e]["WordText"]+"</button></span>";
                                            document.getElementById('modal_txtxx').innerHTML = document.getElementById('modal_txtxx').innerHTML+to_show2;
                                        // }
                                    }
                                }
                            })

                        } else {
                            setTimeout(function () {
                                var datas = "file_name={$features.value}.jpg";
                                var url = "{$urls.base_url}ocr/request.php";
                                $.ajax({
                                    url:url,
                                    method:"POST",
                                    data:datas,
                                    success:function(data)
                                    {
                                        var data_parsed2 = JSON.parse(data);
                                        for(let e=0;e<data_parsed2.length;e++){
                                            // if (parseInt(data_parsed2[e]["pos_Left"]) < 580){
                                                var to_show2 = "<span class='btn_num_eclated' id='span_"+data_parsed2[e]["id"]+"' style='position:absolute;left:"+data_parsed2[e]["pos_Left"]+"px;top:"+data_parsed2[e]["pos_Top"]+"px'>"+data_parsed2[e]["WordText"]+"</button></span>";
                                                document.getElementById('modal_txtxx').innerHTML = document.getElementById('modal_txtxx').innerHTML+to_show2;
                                            // }
                                        }
                                    }
                                })
                            },5000);
                            console.log("Doesn't Work");
                        }
                    // }
                </script>
                {if $features.name == 'Modèle'}
                    <img class="js-modal-product-cover product-cover-modal" width="auto{*$product.cover.large.width*}" src="{$urls.base_url}assets/img_eclated/{$features.value}/{$features.value}.jpg" alt="{$product.cover.legend}" title="{$product.cover.legend}" itemprop="image">
                {/if}
            {/foreach}
          <figcaption class="image-caption">
          {block name='product_description_short'}
            <div id="product-description-short" itemprop="description">{$product.description_short nofilter}</div>
          {/block}
        </figcaption>
        </figure>
        <aside id="thumbnails" class="thumbnails js-thumbnails text-sm-center">
          {block name='product_images'}
            <div class="js-modal-mask mask {if $imagesCount <= 5} nomargin {/if}">
              <ul class="product-images js-modal-product-images">
                {foreach from=$product.images item=image}
                  <li class="thumb-container">
                    <img data-image-large-src="{$image.large.url}" class="thumb js-modal-thumb" src="{$image.medium.url}" alt="{$image.legend}" title="{$image.legend}" width="{$image.medium.width}" itemprop="image">
                  </li>
                {/foreach}
              </ul>
            </div>
          {/block}
          {if $imagesCount > 5}
            <div class="arrows js-modal-arrows">
              <i class="material-icons arrow-up js-modal-arrow-up">&#xE5C7;</i>
              <i class="material-icons arrow-down js-modal-arrow-down">&#xE5C5;</i>
            </div>
          {/if}
        </aside>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<style>
    .modal-dialog{
        max-width: 800px!important;
    }
    #product-modal .modal-content .modal-body .image-caption{
        width: auto!important;
    }
    .btn_num_eclated{
        width: 25px;
        background: black;
        text-align: center;
        opacity: .7;
        color: white;
    }
</style>