{**
 * 2007-2019 PrestaShop and Contributors
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License 3.0 (AFL-3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * https://opensource.org/licenses/AFL-3.0
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to https://www.prestashop.com for more information.
 *
 * @author    PrestaShop SA <contact@prestashop.com>
 * @copyright 2007-2019 PrestaShop SA and Contributors
 * @license   https://opensource.org/licenses/AFL-3.0 Academic Free License 3.0 (AFL-3.0)
 * International Registered Trademark & Property of PrestaShop SA
 *}
<div class="product-add-to-cart">
  <div class="az-qty">
      {if !$configuration.is_catalog}
      {block name="labe_qty_content"}
        <div class="label_qty">
            <span class="control-label">{l s='Quantity' d='Shop.Theme.Catalog'}</span>
        </div>
      {/block}

      {block name='product_quantity'}
        <div class="product-quantity clearfix">
          <div class="qty">
            <input
              type="number"
              name="qty"
              id="quantity_wanted"
              value="{$product.quantity_wanted}"
              class="input-group"
              min="{$product.minimal_quantity}"
              aria-label="{l s='Quantity' d='Shop.Theme.Actions'}"
            >
          </div>
          {hook h='displayProductActions' product=$product}
        </div>
      {/block}
  </div>
  <div class="stock-status-content">
      {block name='product_availability'}
          <div id="product-availability">
            {if $product.show_availability && $product.availability_message}
                {if $product.availability == 'available'}
                    <p class="m-0 status-en-stock">
                        <i class="material-icons rtl-no-flip product-available">&#xE5CA;</i>
                        {l s="En stock" d='Shop.Theme.Actions'}
                    </p>
                    <p class="m-0 text-en-stock">{$product.delivery_in_stock}</p>
                {elseif $product.availability == 'last_remaining_items'}
                    <p class="m-0 status-en-stock">
                        <i class="material-icons product-last-items">&#xE002;</i>
                        {l s="Derniers articles en stock" d='Shop.Theme.Actions'}
                    </p>
                    <p class="m-0 text-en-stock">{$product.delivery_in_stock}</p>
                {else}
                    <p class="m-0 status-en-stock">
                        <i class="material-icons product-unavailable">&#xE14B;</i>
                        {l s="En rupture de stock" d='Shop.Theme.Actions'}
                    </p>
                    <p class="m-0 text-en-stock">{$product.delivery_out_stock}</p>
                {/if}
            {/if}
          </div>
      {/block}
    {*{if $product.quantity === 0}*}
        {*<p class="m-0 status-en-stock" id="product-availability"><i class="material-icons product-unavailable">&#xE14B;</i>{$product.availability_message}</p>*}
        {*<p class="m-0 text-en-stock">{$product.delivery_out_stock}</p>*}
    {*{else}*}
        {*<p class="m-0 status-en-stock"><i class="fa fa-check az-stock-icon"></i><span class="status-stock-text"> En stock</span></p>*}
        {*<p class="m-0 text-en-stock">{$product.delivery_in_stock}</p>*}
    {*{/if}*}

  </div>

    {block name='product_quantity'}
        <div class="add">
          <button
            class="btn btn-primary add-to-cart"
            data-button-action="add-to-cart"
            type="submit"
            {if !$product.add_to_cart_url}
              disabled
            {/if}
          >
            <i class="material-icons shopping-cart">&#xE547;</i>
            {l s='Add to cart' d='Shop.Theme.Actions'}
          </button>
        </div>

        {hook h='displayProductActions' product=$product}
      </div>
    {/block}

    {block name='product_minimal_quantity'}
      <p class="product-minimal-quantity">
        {if $product.minimal_quantity > 1}
          {l
          s='The minimum purchase order quantity for the product is %quantity%.'
          d='Shop.Theme.Checkout'
          sprintf=['%quantity%' => $product.minimal_quantity]
          }
        {/if}
      </p>
    {/block}
  {/if}
</div>
