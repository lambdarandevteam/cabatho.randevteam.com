{**
 * 2007-2019 PrestaShop and Contributors
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Academic Free License 3.0 (AFL-3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * https://opensource.org/licenses/AFL-3.0
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to https://www.prestashop.com for more information.
 *
 * @author    PrestaShop SA <contact@prestashop.com>
 * @copyright 2007-2019 PrestaShop SA and Contributors
 * @license   https://opensource.org/licenses/AFL-3.0 Academic Free License 3.0 (AFL-3.0)
 * International Registered Trademark & Property of PrestaShop SA
 *}
{extends file=$layout}

{block name='head_seo' prepend}
  <link rel="canonical" href="{$product.canonical_url}">
{/block}

{block name='head' append}
  <meta property="og:type" content="product">
  <meta property="og:url" content="{$urls.current_url}">
  <meta property="og:title" content="{$page.meta.title}">
  <meta property="og:site_name" content="{$shop.name}">
  <meta property="og:description" content="{$page.meta.description}">
  <meta property="og:image" content="{$product.cover.large.url}">
  {if $product.show_price}
    <meta property="product:pretax_price:amount" content="{$product.price_tax_exc}">
    <meta property="product:pretax_price:currency" content="{$currency.iso_code}">
    <meta property="product:price:amount" content="{$product.price_amount}">
    <meta property="product:price:currency" content="{$currency.iso_code}">
  {/if}
  {if isset($product.weight) && ($product.weight != 0)}
  <meta property="product:weight:value" content="{$product.weight}">
  <meta property="product:weight:units" content="{$product.weight_unit}">
  {/if}
{/block}

{block name='content'}

  <section id="main" itemscope itemtype="https://schema.org/Product">
    <meta itemprop="url" content="{$product.url}">
        {*{dump($product_piece)}*}
      {foreach from=$product.grouped_features item=features}
          {if $features.id_feature == 5}
              {if (!isset($product_piece) || $product_piece == []) && $features.value == "Produit à réparer"}
                  <div class="row p-2 azrow">
                      <div class="col-md-4 p-2 azview az-left">
                          {block name='page_content_container'}
                              <section class="page-content" id="content">
                                  {block name='page_content'}
                                      <!-- @todo: use include file='catalog/_partials/product-flags.tpl'} -->

                                      {block name='product_cover_thumbnails'}
                                          {include file='catalog/_partials/product-cover-thumbnails.tpl'}
                                      {/block}
                                      <div class="scroll-box-arrows">
                                          <i class="material-icons left">&#xE314;</i>
                                          <i class="material-icons right">&#xE315;</i>
                                      </div>

                                  {/block}
                              </section>
                          {/block}
                      </div>
                      <div class="col-md-5 pt-2 azview az-center">
                          {block name='page_header_container'}
                              {block name='page_header'}
                                  <h1 class="h1" itemprop="name">{block name='page_title'}{$product.name}{/block}</h1>
                              {/block}

                              {block name='product_reference'}
                                  {if isset($product_manufacturer->id)}
                                      <div class="product-manufacturer">
                                          {if isset($manufacturer_image_url)}
                                              <a href="{$product_brand_url}">
                                                  <img src="{$manufacturer_image_url}" class="img img-thumbnail manufacturer-logo" alt="{$product_manufacturer->name}" width="60" height="50">
                                              </a>
                                          {else}
                                              <label class="label">{l s='Brand' d='Shop.Theme.Catalog'}</label>
                                              <span>
                                                <a href="{$product_brand_url}">{$product_manufacturer->name}</a>
                                              </span>
                                          {/if}
                                      </div>
                                  {/if}
                                  {*{if isset($product.reference_to_display) && $product.reference_to_display neq ''}*}
                                  {*<div class="product-reference">*}
                                  {*<label class="label">{l s='Reference' d='Shop.Theme.Catalog'} </label>*}
                                  {*<span itemprop="sku">{$product.reference_to_display}</span>*}
                                  {* <img class="img-fluid" src="{$link->getManufacturerImageLink($product.id_manufacturer, 'small_default')}" alt="image-{$product.id_manufacturer}" width="60" height="50"> *}
                                  {*</div>*}
                                  {*{/if}*}

                                  {foreach from=$product.features item=feature}
                                      {if $feature.id_feature == 4}
                                          <div class="product-reference">
                                              <label class="label label_product_desc">{l s='Référence :' d='Shop.Theme.Catalog'} </label>
                                              <span class="modele_product_categ">{$feature.value}</span>
                                              {* <img class="img-fluid" src="{$link->getManufacturerImageLink($product.id_manufacturer, 'small_default')}" alt="image-{$product.id_manufacturer}" width="60" height="50"> *}
                                          </div>
                                      {/if}
                                  {/foreach}
                              {/block}

                              {* if product have specific references, a table will be added to product details section *}
                              {block name='product_specific_references'}
                                  {if !empty($product.specific_references)}
                                      <section class="product-features">
                                          <p class="h6">{l s='Specific References' d='Shop.Theme.Catalog'}</p>
                                          <dl class="data-sheet">
                                              {foreach from=$product.specific_references item=reference key=key}
                                                  <dt class="name">{$key}</dt>
                                                  <dd class="value">{$reference}</dd>
                                              {/foreach}
                                          </dl>
                                      </section>
                                  {/if}
                              {/block}

                              {block name='product_type'}
                                  {if !empty($product.features)}
                                      <div class="product-type-content">
                                          <p class="product-type label_product_desc">Type de produit : </p>
                                          {*<p class="product-type-value">&nbsp;{$product.features[1].value}</p>*}
                                          {foreach from=$product.features item=feature}
                                              {*{$feature|dump}*}
                                              {if $feature.id_feature == 3}
                                                  <p class="product-type-value">{$feature.value}</p>
                                              {/if}
                                          {/foreach}
                                      </div>
                                  {/if}
                              {/block}

                              {block name='product_description'}
                                  <p class="desc-label label_product_desc">Description :</p>
                                  <div class="product-description">{$product.description nofilter}</div>
                              {/block}
                          {/block}
                      </div>
                      <div class="col-md-3 p-2 azview az-right">
                          <div class="az-right-content pt-3">
                              {block name='product_prices'}
                                  {include file='catalog/_partials/product-prices.tpl'}
                              {/block}
                              <div class="product-actions">
                                  {block name='product_buy'}
                                      <form action="{$urls.pages.cart}" method="post" id="add-to-cart-or-refresh">
                                          <input type="hidden" name="token" value="{$static_token}">
                                          <input type="hidden" name="id_product" value="{$product.id}" id="product_page_product_id">
                                          <input type="hidden" name="id_customization" value="{$product.id_customization}" id="product_customization_id">

                                          {block name='product_variants'}
                                              {include file='catalog/_partials/product-variants.tpl'}
                                          {/block}

                                          {block name='product_pack'}
                                              {if $packItems}
                                                  <section class="product-pack">
                                                      <p class="h4">{l s='This pack contains' d='Shop.Theme.Catalog'}</p>
                                                      {foreach from=$packItems item="product_pack"}
                                                          {block name='product_miniature'}
                                                              {include file='catalog/_partials/miniatures/pack-product.tpl' product=$product_pack}
                                                          {/block}
                                                      {/foreach}
                                                  </section>
                                              {/if}
                                          {/block}

                                          {block name='product_discounts'}
                                              {include file='catalog/_partials/product-discounts.tpl'}
                                          {/block}

                                          {block name='product_add_to_cart'}
                                              {include file='catalog/_partials/product-add-to-cart.tpl'}
                                          {/block}

                                          {* Input to refresh product HTML removed, block kept for compatibility with themes *}
                                          {block name='product_refresh'}{/block}
                                      </form>
                                  {/block}

                              </div>
                          </div>
                      </div>
                  </div>

              {elseif (isset($product_piece) && $product_piece != []) && $features.value == "Pièce détachée"}

                  <div class="row p-2 azrow">
                      <div class="col-md-4 p-2 azview az-left">
                          {block name='page_content_container'}
                              <section class="page-content" id="content">
                                  {block name='page_content'}
                                      <!-- @todo: use include file='catalog/_partials/product-flags.tpl'} -->

                                      {block name='product_cover_thumbnails'}
                                          {include file='catalog/_partials/product-cover-thumbnails.tpl'}
                                      {/block}
                                      <div class="scroll-box-arrows">
                                          <i class="material-icons left">&#xE314;</i>
                                          <i class="material-icons right">&#xE315;</i>
                                      </div>

                                  {/block}
                              </section>
                          {/block}
                      </div>
                      <div class="col-md-5 pt-2 azview az-center">
                          {block name='page_header_container'}
                              {block name='page_header'}
                                  <h1 class="h1" itemprop="name">{block name='page_title'}{$product.name}{/block}</h1>
                              {/block}

                              {block name='product_reference'}
                                  {if isset($product_manufacturer->id)}
                                      <div class="product-manufacturer">
                                          {if isset($manufacturer_image_url)}
                                              <a href="{$product_brand_url}">
                                                  <img src="{$manufacturer_image_url}" class="img img-thumbnail manufacturer-logo" alt="{$product_manufacturer->name}" width="60" height="50">
                                              </a>
                                          {else}
                                              <label class="label">{l s='Brand' d='Shop.Theme.Catalog'}</label>
                                              <span>
                        <a href="{$product_brand_url}">{$product_manufacturer->name}</a>
                      </span>
                                          {/if}
                                      </div>
                                  {/if}
                                  {*{if isset($product.reference_to_display) && $product.reference_to_display neq ''}*}
                                  {*<div class="product-reference">*}
                                  {*<label class="label">{l s='Reference' d='Shop.Theme.Catalog'} </label>*}
                                  {*<span itemprop="sku">{$product.reference_to_display}</span>*}
                                  {* <img class="img-fluid" src="{$link->getManufacturerImageLink($product.id_manufacturer, 'small_default')}" alt="image-{$product.id_manufacturer}" width="60" height="50"> *}
                                  {*</div>*}
                                  {*{/if}*}

                                  {foreach from=$product.features item=feature}
                                      {if $feature.id_feature == 4}
                                          <div class="product-reference">
                                              <label class="label">{l s='Référence :' d='Shop.Theme.Catalog'} </label>
                                              <span class="modele_product_categ">{$feature.value}</span>
                                              {* <img class="img-fluid" src="{$link->getManufacturerImageLink($product.id_manufacturer, 'small_default')}" alt="image-{$product.id_manufacturer}" width="60" height="50"> *}
                                          </div>
                                      {/if}
                                  {/foreach}
                              {/block}

                              {* if product have specific references, a table will be added to product details section *}
                              {block name='product_specific_references'}
                                  {if !empty($product.specific_references)}
                                      <section class="product-features">
                                          <p class="h6">{l s='Specific References' d='Shop.Theme.Catalog'}</p>
                                          <dl class="data-sheet">
                                              {foreach from=$product.specific_references item=reference key=key}
                                                  <dt class="name">{$key}</dt>
                                                  <dd class="value">{$reference}</dd>
                                              {/foreach}
                                          </dl>
                                      </section>
                                  {/if}
                              {/block}

                              {block name='product_type'}
                                  {if !empty($product.features)}
                                      <div class="product-type-content">
                                          <p class="product-type">Type de piéce : </p>
                                          {*<p class="product-type-value">&nbsp;{$product.features[1].value}</p>*}
                                          {foreach from=$product.features item=feature}
                                              {*{$feature|dump}*}
                                              {if $feature.id_feature == 3}
                                                  <p class="product-type-value">{$feature.value}</p>
                                              {/if}
                                          {/foreach}
                                      </div>
                                  {/if}
                              {/block}

                              {block name='product_description'}
                                  <p class="desc-label">Caractéritiques :</p>
                                  <div class="product-description">{$product.description nofilter}</div>
                              {/block}
                          {/block}
                      </div>
                      <div class="col-md-3 p-2 azview az-right">
                          <div class="az-right-content pt-3">
                              {block name='product_prices'}
                                  {include file='catalog/_partials/product-prices.tpl'}
                              {/block}
                              <div class="product-actions">
                                  {block name='product_buy'}
                                      <form action="{$urls.pages.cart}" method="post" id="add-to-cart-or-refresh">
                                          <input type="hidden" name="token" value="{$static_token}">
                                          <input type="hidden" name="id_product" value="{$product.id}" id="product_page_product_id">
                                          <input type="hidden" name="id_customization" value="{$product.id_customization}" id="product_customization_id">

                                          {block name='product_variants'}
                                              {include file='catalog/_partials/product-variants.tpl'}
                                          {/block}

                                          {block name='product_pack'}
                                              {if $packItems}
                                                  <section class="product-pack">
                                                      <p class="h4">{l s='This pack contains' d='Shop.Theme.Catalog'}</p>
                                                      {foreach from=$packItems item="product_pack"}
                                                          {block name='product_miniature'}
                                                              {include file='catalog/_partials/miniatures/pack-product.tpl' product=$product_pack}
                                                          {/block}
                                                      {/foreach}
                                                  </section>
                                              {/if}
                                          {/block}

                                          {block name='product_discounts'}
                                              {include file='catalog/_partials/product-discounts.tpl'}
                                          {/block}

                                          {block name='product_add_to_cart'}
                                              {include file='catalog/_partials/product-add-to-cart.tpl'}
                                          {/block}

                                          {* Input to refresh product HTML removed, block kept for compatibility with themes *}
                                          {block name='product_refresh'}{/block}
                                      </form>
                                  {/block}

                              </div>
                          </div>
                      </div>
                  </div>

              {else}

                  <div class="row">
                      <div class="col-md-6">
                          {block name='page_content_container'}
                              <section class="page-content" id="content">
                                  {block name='page_content'}
                                      <!-- @todo: use include file='catalog/_partials/product-flags.tpl'} -->
                                      {block name='product_flags'}
                                          <ul class="product-flags">
                                              {foreach from=$product.flags item=flag}
                                                  <li class="product-flag {$flag.type}">{$flag.label}</li>
                                              {/foreach}
                                          </ul>
                                      {/block}

                                      {block name='product_cover_thumbnails'}
                                          {include file='catalog/_partials/product-cover-thumbnails-eclated.tpl'}
                                      {/block}
                                      <div class="scroll-box-arrows">
                                          <i class="material-icons left">&#xE314;</i>
                                          <i class="material-icons right">&#xE315;</i>
                                      </div>

                                  {/block}
                              </section>
                          {/block}
                      </div>
                      <div class="col-md-6 border_separator">
                          {block name='page_header_container'}
                              {block name='page_header'}
                                  <h1 class="h1" itemprop="name">{block name='page_title'}{$product.name}{/block}</h1>
                              {/block}
                          {/block}


                          <div class="product-information">
                              {block name='product_description_short'}
                                  <div id="product-description-short-{$product.id}" itemprop="description">{$product.description_short nofilter}</div>
                              {/block}

                              {if $product.is_customizable && count($product.customizations.fields)}
                                  {block name='product_customization'}
                                      {include file="catalog/_partials/product-customization.tpl" customizations=$product.customizations}
                                  {/block}
                              {/if}

                              <div class="product-actions">
                                  {block name='product_buy'}
                                      <form action="{$urls.pages.cart}" method="post" id="add-to-cart-or-refresh">
                                          <input type="hidden" name="token" value="{$static_token}">
                                          <input type="hidden" name="id_product" value="{$product.id}" id="product_page_product_id">
                                          <input type="hidden" name="id_customization" value="{$product.id_customization}" id="product_customization_id">

                                          {*{block name='product_variants'}*}
                                              {*{include file='catalog/_partials/product-variants.tpl'}*}
                                          {*{/block}*}

                                          {block name='product_pack'}
                                              {if $packItems}
                                                  <section class="product-pack">
                                                      <p class="h4">{l s='This pack contains' d='Shop.Theme.Catalog'}</p>
                                                      {foreach from=$packItems item="product_pack"}
                                                          {block name='product_miniature'}
                                                              {include file='catalog/_partials/miniatures/pack-product.tpl' product=$product_pack}
                                                          {/block}
                                                      {/foreach}
                                                  </section>
                                              {/if}
                                          {/block}


                                          {* Input to refresh product HTML removed, block kept for compatibility with themes *}
                                          {block name='product_refresh'}{/block}
                                      </form>
                                  {/block}

                              </div>
                              <div id="btn_reload" class="pb-4"><button onclick="reinitialise()" class="btn btn-info">Reinitialiser</button></div>
                              {block name='product_tabs'}
                                  {assign var="keyss" value= 0}
                                  {foreach from=$product_piece item="piece"}
                                      <div class="row piece_one_content pb-2" id="piece_one_content{foreach from=$piece.grouped_features item="featurer"}{if $featurer.name =='repere vue eclate'}{$featurer.value}{/if}{/foreach}">
                                            {*{$piece|dump}*}
                                          <div class="col-lg-3">
                                              {if $piece.image != false}
                                                  <img src="{$urls.base_url}{$piece.imagepath}" class="img-fluid">
                                              {else}
                                                  <img src="{$urls.img_url}noimage.png" class="img-fluid">
                                              {/if}
                                              {foreach from=$piece.grouped_features item="featurer"}
                                                  {if $featurer.name =='repere vue eclate'}
                                                      <span class="num_piece">{$featurer.value}</span>
                                                  {/if}
                                              {/foreach}
                                          </div>
                                          <div class="col-lg-6">
                                              <div class="w-100" :class="piece_name">
                                                  <a href="{$piece.link}">{*{$piece.link}*}
                                                      <span class="title_vue_eclat">{$piece.name}</span>
                                                  </a>
                                              </div>
                                              <div class="w-100" :class="reference_piece">
                                                  {foreach from=$piece.grouped_features item="featurer"}
                                                      {if $featurer.id_feature == 4}
                                                          <span class="ref_vue_eclat">Référence : </span>
                                                          <span class="modele_vue_eclat">{$featurer.value}</span>
                                                      {/if}
                                                  {/foreach}
                                              </div>
                                              {*<div class="w-100" :class="brand_piece">*}
                                                  {*{foreach from=$piece.grouped_features item="featurer"}*}
                                                      {*{if $featurer.name =='Référence pièce'}*}
                                                          {*Référence : {$featurer.value}*}
                                                      {*{/if}*}
                                                  {*{/foreach}*}
                                              {*</div>*}
                                              <div class="row" :class="reference_piece">
                                                  <div class="col-5">
                                                      <img src="{$piece.manufacturer_img_url}" class="img-fluid">
                                                  </div>
                                                  <div class="col-lg-7 middle_content">
                                                      {if $piece.quantity == 0}
                                                          <div class="w-100 txt_stock_rupture" id="product-availability">
                                                              <i class="material-icons product-unavailable">&#xE14B;</i>
                                                              <span class="status-en-stock-eclat">{l s="En rupture de stock" d='Shop.Theme.Actions'}</span>
                                                          </div>
                                                          <div class="w-100 txt_en_livr" >
                                                              <span class="status-en-stock-eclat">{$piece.delivery_out_stock}</span>
                                                          </div>
                                                      {elseif $piece.quantity == 1}
                                                          <div class="w-100 txt_stock_rupture" id="product-availability">
                                                              <i class="material-icons product-last-items">&#xE002;</i>
                                                              <span class="status-en-stock-eclat">{l s="Derniers articles en stock" d='Shop.Theme.Actions'}</span>
                                                          </div>
                                                          <div class="w-100 txt_en_livr" >
                                                              <span class="status-en-stock-eclat">{$piece.delivery_in_stock}</span>
                                                          </div>
                                                      {else}
                                                          <div class="w-100 txt_en_stock" id="product-availability">
                                                              <i class="material-icons rtl-no-flip product-available">&#xE5CA;</i>
                                                              <span class="status-en-stock-eclat">{l s="En stock" d='Shop.Theme.Actions'}</span>
                                                          </div>
                                                          <div class="w-100 txt_en_livr" >
                                                              <span class="status-en-stock-eclat">{$piece.delivery_in_stock}</span>
                                                          </div>
                                                      {/if}
                                                  </div>
                                              </div>
                                          </div>
                                          {*{dump($piece)}*}
                                          <div class="col-lg-3">
                                              <div class="w-100 price_content">
                                                  <span class="price_txt">{$piece.price}</span><span class="ttc_txt">€TTC</span>
                                              </div>
                                              <div class="w-100 detail_content">
                                                  {*<a href="{$piece.link}"><span class="price_txt">En savoir plus</span></a>*}
                                                  {if $piece.description != ""}
                                                      <a href="javascript:void(0)" onmouseenter="show_desc({$piece.id_product})" onmouseout="hide_desc({$piece.id_product})">
                                                          <i class="material-icons" style="color:#36B0D2">info</i>
                                                          {*<span class="info_icon">i</span>*}
                                                          <span class="view_more">En savoir plus</span>
                                                      </a>
                                                      <div class="description_piece" id="piece_{$piece.id_product}">{$piece.description nofilter}</div>
                                                  {/if}
                                              </div>
                                              <div class="w-100 detail_add_to_cart">

                                                  <!-- Button trigger modal -->
                                                  <button  type="button" class="btn btn_add_cart_piece" data-toggle="modal" data-target="#exampleModalLong">
                                                      {l s='Add to cart' d='Shop.Theme.Actions'}
                                                  </button>

                                                  <!-- Modal -->
                                                  <div class="modal fade" id="exampleModalLong" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
                                                      <div class="modal-dialog" role="document">
                                                          <div class="modal-content">
                                                              <div class="modal-header">
                                                                  <h5 class="modal-title" id="exampleModalLongTitle">{$piece.name}</h5>
                                                                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                      <span aria-hidden="true">&times;</span>
                                                                  </button>
                                                              </div>
                                                              <div class="modal-body">
                                                                  <form action="{$urls.pages.cart}" method="post" id="add-to-cart-or-refresh">
                                                                      <input type="hidden" name="token" value="{$static_token}">
                                                                      <input type="hidden" name="id_product" value="{$piece.id_product}" id="product_page_product_id">
                                                                      {*<input type="hidden" name="id_customization" value="{$piece.id_customization}" id="product_customization_id">*}

                                                                      {block name='product_add_to_cart'}
                                                                          {include file='catalog/_partials/product-add-to-cart_piece.tpl'}
                                                                      {/block}

                                                                      {* Input to refresh product HTML removed, block kept for compatibility with themes *}
                                                                      {block name='product_refresh'}{/block}
                                                                  </form>
                                                              </div>
                                                              <div class="modal-footer">
                                                                  <button id="modal_close" type="button" class="btn btn-secondary" data-dismiss="modal">Fermer</button>
                                                              </div>
                                                              <script type="text/javascript">
                                                                  function reinitialise() {
                                                                      location.reload();
                                                                  }
                                                                  if (window.jQuery) {
                                                                      $(document).ready(function () {
                                                                          $("#modal_close").click(function () {
                                                                              location.reload();
                                                                          })
                                                                      })

                                                                  } else {
                                                                      setTimeout(function () {
                                                                          $(document).ready(function () {
                                                                              $("#modal_close").click(function () {
                                                                                  location.reload();
                                                                              })
                                                                          })
                                                                      },5000)
                                                                  }
                                                              </script>
                                                          </div>
                                                      </div>
                                                  </div>
                                              </div>
                                          </div>
                                      </div>
                                      {$keyss = $keyss+1}
                                  {/foreach}
                                  <script type="text/javascript">
                                      function show_desc(id){
                                          document.getElementById('piece_'+id).style.display ="block";
                                      }
                                      function hide_desc(id){
                                          document.getElementById('piece_'+id).style.display ="none";
                                      }
                                  </script>
                              {/block}
                          </div>
                      </div>
                  </div>

              {/if}
          {/if}
      {/foreach}

    {block name='product_accessories'}
      {if $accessories}
        <section class="product-accessories clearfix">
            <h1 class="text-center titre_block_engage pt-4 title_bloc">
                {l s='Profitez des accessoires' d='Modules.Featuredaccessory.Shop'}
            </h1>
            <p class="text-center texte_block_engage pb-4 sous-title">
                {l s='au meilleur prix' d='Modules.Featuredaccessory.Shop'}
            </p>
          <div class="products">
            {foreach from=$accessories item="product_accessory"}
              {block name='product_miniature'}
                {include file='catalog/_partials/miniatures/productaccessory.tpl' product=$product_accessory}
              {/block}
            {/foreach}
          </div>
        </section>
      {/if}
    {/block}

    {block name='product_footer'}
      {hook h='displayFooterProduct' product=$product category=$category}
    {/block}
    {if (!isset($product_piece) || $product_piece == [])}
        {block name='product_images_modal'}
            {include file='catalog/_partials/product-images-modal.tpl'}
        {/block}
    {else}
        {block name='product_images_modal'}
          {include file='catalog/_partials/product-images-modal-eclated.tpl'}
        {/block}
    {/if}
    {block name='page_footer_container'}
      <footer class="page-footer">
        {block name='page_footer'}
          <!-- Footer content -->
        {/block}
      </footer>
    {/block}
  </section>

{/block}
