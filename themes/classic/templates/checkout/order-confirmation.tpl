{extends file='page.tpl'}

{block name='page_content_container' prepend}
    <section id="content-hook_order_confirmation" class="card">
      <div class="card-block">
        <div class="row">
          <div class="col-md-12">

            {block name='order_confirmation_header'}
              <h3 class="h1 card-title">
                <i class="material-icons rtl-no-flip done">&#xE876;</i>{l s='Your order is confirmed' d='Shop.Theme.Checkout'}
              </h3>
            {/block}

            <p>
              {l s='An email has been sent to your mail address %email%.' d='Shop.Theme.Checkout' sprintf=['%email%' => $customer.email]}
              {if $order.details.invoice_url}
                {* [1][/1] is for a HTML tag. *}
                {l
                  s='You can also [1]download your invoice[/1]'
                  d='Shop.Theme.Checkout'
                  sprintf=[
                    '[1]' => "<a href='{$order.details.invoice_url}'>",
                    '[/1]' => "</a>"
                  ]
                }
              {/if}
            </p>

            {block name='hook_order_confirmation'}
              {$HOOK_ORDER_CONFIRMATION nofilter}
            {/block}

          </div>
        </div>
      </div>
    </section>
{/block}

{block name='page_content_container'}
  <div class="text-center m-4"><h1 style="font-size:25px;font-family:Muli-ExtraBold;text-transform: UPPERCASE;;">Récapitulatif de commande</h1></div>
  <section id="content" class="page-content page-order-confirmation card">
    <div class="card-block" style="padding: 0;">
      <div class="row">

        {block name='order_confirmation_table'}
          {include
            file='checkout/_partials/order-confirmation-table.tpl'
            products=$order.products
            subtotals=$order.subtotals
            totals=$order.totals
            labels=$order.labels
            add_product_link=false
          }
        {/block}

        {* {block name='order_details'}
          <div id="order-details" class="col-md-4">
            <h3 class="h3 card-title">{l s='Order details' d='Shop.Theme.Checkout'}:</h3>
            <ul>
              <li>{l s='Order reference: %reference%' d='Shop.Theme.Checkout' sprintf=['%reference%' => $order.details.reference]}</li>
              <li>{l s='Payment method: %method%' d='Shop.Theme.Checkout' sprintf=['%method%' => $order.details.payment]}</li>
              {if !$order.details.is_virtual}
                <li>
                  {l s='Shipping method: %method%' d='Shop.Theme.Checkout' sprintf=['%method%' => $order.carrier.name]}<br>
                  <em>{$order.carrier.delay}</em>
                </li>
              {/if}
            </ul>
          </div>
        {/block} *}
      

      </div>
    </div>
          {*Faniry  *}
        <div class="text-center m-5">
          <h4>Adresse de livraison</h4>
        </div>
        <div class="row">
          <div class="col-6">
            <div class="text-center" style="border: 1px solid #6666;margin:auto;padding: 10%;">
              <h3>Adresse de livraison</h3>
              <p>
                M. Alain Dupond
                5 rue des lilas
                49125 Briollay FRA
                (+33) 06023569874 
              </p>
            </div
          </div>
          </div>
          <div class="col-6">
            <div class="text-center" style="border: 1px solid #6666;margin:auto;padding: 10%;">
                <h3>Adresse de livraison</h3>
                <p>
                  M. Alain Dupond
                  5 rue des lilas
                  49125 Briollay FRA
                  (+33) 06023569874 
                </p>
              </div
            </div>            
        </div>
        </div>
        <div class="text-center m-5 prc">
          <h4>Moyen de livraison</h4>
        </div>
        <hr style="border-top: 1px dashed #66666;" class="mr-3 ml-3">
        <div class="text-center m-5 prc">
          <h4>Moyen de livraison</h4>
        </div>
        <hr style="border-top: 1px dashed #66666;" class="mr-3 ml-3">
        <div class="text-center m-5 prc">
          <h4>conditions générales de vente</h4>
        </div>
      {* End *}
  </section>

  {block name='hook_payment_return'}
    {if ! empty($HOOK_PAYMENT_RETURN)}
    <section id="content-hook_payment_return" class="card definition-list">
      <div class="card-block">
        <div class="row">
          <div class="col-md-12">
            {$HOOK_PAYMENT_RETURN nofilter}
          </div>
        </div>
      </div>
    </section>
    {/if}
  {/block}

  {block name='customer_registration_form'}
    {if $customer.is_guest}
      <div id="registration-form" class="card">
        <div class="card-block">
          <h4 class="h4">{l s='Save time on your next order, sign up now' d='Shop.Theme.Checkout'}</h4>
          {render file='customer/_partials/customer-form.tpl' ui=$register_form}
        </div>
      </div>
    {/if}
  {/block}

  {block name='hook_order_confirmation_1'}
    {hook h='displayOrderConfirmation1'}
  {/block}

  {block name='hook_order_confirmation_2'}
    <section id="content-hook-order-confirmation-footer">
      {hook h='displayOrderConfirmation2'}
    </section>
  {/block}
{/block}
