<?php
/**
 * 2007-2019 PrestaShop and Contributors
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * https://opensource.org/licenses/OSL-3.0
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@prestashop.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade PrestaShop to newer
 * versions in the future. If you wish to customize PrestaShop for your
 * needs please refer to https://www.prestashop.com for more information.
 *
 * @author    PrestaShop SA <contact@prestashop.com>
 * @copyright 2007-2019 PrestaShop SA and Contributors
 * @license   https://opensource.org/licenses/OSL-3.0 Open Software License (OSL 3.0)
 * International Registered Trademark & Property of PrestaShop SA
 */

$rand_uri_request = "$_SERVER[REQUEST_URI]";
$pos = strpos($rand_uri_request, "/brand/");
if ($pos !== false) {
	//echo "FOUND";
	$rand_uri_request = str_replace("/brand/","",$rand_uri_request);
	//var_dump($rand_uri_request);
	$pieces_rand = explode("-", $rand_uri_request);
	//var_dump($pieces_rand[0]);
	if(is_numeric($pieces_rand[0]) && $pieces_rand[0] < 10){
		$rand_uri_request = substr($rand_uri_request, 2);
	} else if(is_numeric($pieces_rand[0]) && $pieces_rand[0] < 100){
		$rand_uri_request = substr($rand_uri_request, 3);
	} else if(is_numeric($pieces_rand[0]) && $pieces_rand[0] < 1000){
		$rand_uri_request = substr($rand_uri_request, 4);
	} else {
		$rand_uri_request = substr($rand_uri_request, 5);
	}
	//var_dump($rand_uri_request);
	$rand_uri_request = str_replace("-","--",$rand_uri_request);
	header("Location: /3-nos-univers?q=Marque-".$rand_uri_request);
}

require dirname(__FILE__).'/config/config.inc.php';
Dispatcher::getInstance()->dispatch();
